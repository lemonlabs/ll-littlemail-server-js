express     = require 'express'
socketio    = require 'socket.io'
http        = require 'http'

app = express()
server = http.createServer(app)
io = socketio.listen(server)
app.use express.bodyParser()

socket = undefined

friends =
  "1234":
    id: "1234"
    name: "Mindaugas Kuprionis"
    picture: "https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-ash3/486805_10151456591757881_949970517_n.jpg"
  "64344":
    id: "64344"
    name: "Jonas Lekevičius"
    picture: "https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-ash3/1012491_10151789379804621_990242878_n.jpg"
  "345345":
    id: "345345"
    name: "Marius Kažemėkaitis"
    picture: "https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-ash2/389414_10150476421312982_1940939047_n.jpg"
  "64435":
    id: "64435"
    name: "Balys Valentukevičius"
    picture: "https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-frc3/1269388_10151634328072826_1380782503_o.jpg"

app.get '/', (req, res) ->
  res.send('LittleMail is waiting for your message')

app.post '/auth', (req, res) ->
  res.send(token: "1234567yrtfg")	# no time for real stuff..

app.get '/friends', (req, res) ->
  fr = (f for id, f of friends)
  res.send {
    friends: fr
  }

app.post '/friends/:id/message', (req, res) ->
  if socket?
    socket.emit 'print',
      "name": friends[req.params.id].name
      "message": req.body.message

  res.send {status: "ok"}

io.sockets.on 'connection', (s) ->
  socket = s

  socket.on 'pong', ->
    console.log 'got pong'

  socket.emit 'ping', {}

server.listen process.env.PORT || 3000

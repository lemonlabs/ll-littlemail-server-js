superagent = require 'superagent'
expect	   = require 'expect.js'

apiRoot = 'http://127.0.0.1:3000'

describe 'Little Mail API server', ->

  it 'authenticates', (done) ->
    url = apiRoot + '/auth'

    superagent
      .post(url)
      .send(
        facebook_token: 'asdsdfsf' 
      )
      .end( (e, res) ->
        expect(e).to.eql(null)
        expect(res.body).not.to.eql(null)
        expect(res.body.token).not.to.eql(undefined)

        console.log 'POST ' + url
        console.log res.body
        console.log ''
        done()
      )

  it 'gets friend list', (done) ->
    url = apiRoot + '/friends'

    superagent
      .get(url)
      .end( (e, res) ->
        expect(e).to.eql(null)
        expect(res.body).not.to.eql(null)
        expect(res.body).to.be.an('object')
        expect(res.body.friends).to.be.an('array')       

        console.log 'GET ' + url
        console.log res.body
        console.log ''
        done()
      )  
  
  it 'sends a message', (done) ->
    url = apiRoot + '/friends/1234/message'

    superagent
      .post(url)
      .send(
        message: 'asdfgdg'
      )
      .end( (e, res) ->
        expect(e).to.eql(null)
        expect(res.body).to.eql({})

        console.log 'POST ' + url
        console.log res.body
        console.log ''
        done()
      )  